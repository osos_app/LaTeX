# LaTeX - Vorlage für Mathematikschularbeiten und Skripten

## Einleitung:

In der Datei **Vorlage.tex** findet man eine Vorlage zum Erstellen von Mathematik - Schularbeiten und Mathematik - Skripten.

Die Vorlage beruht auf der Exam - Umgebung, deren Dokumentation hier [https://math.mit.edu/~psh/exam/examdoc.pdf](https://math.mit.edu/~psh/exam/examdoc.pdf) zu finden ist.

Die Exam - Umgebung wurde um die Option für

- gerahmte, färbbare Kästchenfelder zum Eintragen von Lösungen 

und um

- "Österreichische" Darstellungen für standartisierte  Multiple - Choice - Aufgaben 

ergänzt.

## Vorschlag bzgl. "Best Practice":

- Alle Prüfungsaufgaben befinden sich innerhalb von
 **\begin{questions} ... \end{questions}**

- Werden die einzelne Aufgaben innerhalb einer Minipage **\begin{minipage} ... \end{minipage}** plaziert, werden sich diese jeweils auf der selben Seite dargestellt

- Fragen mit Punkten innerhalb der Minipage mit **\question[10]**

- Fragen mit Untereilungen innerhalb der Minipage mit **\question** ohne Punkte dafür Unterteilungen **\parts[4]** mit Punkten. 
ACHTUNG: Punkte können nicht bei \questions & \parts gleichzeitig eingetragen werden, da sie sonst in Punktetabelle doppelt gewertet werden

- Kästchenfelder zum Eintragen von Lösungen bei den jeweiligen Prüfungsfragen können durch Auskommentieren des jeweils gewünschten myFillWithGrid - Kommandos ein bzw. ausgeschaltet werde:
  - ein: **%\newcommand \myFillWithGrid[1]{}** und **\newcommand \myFillWithGrid[1]{ ... }** 
  - aus: **\newcommand \myFillWithGrid[1]{}** und **%\newcommand \myFillWithGrid[1]{ ... }** 
  
- Optional können Lösungen zu den Fragen jeweils innerhalb **\begin{solution} ... \end{solution}** erstellt werden. Sinnvollerweise ebenfalls innerhalb der Minipage platziert. Deren Anzeige kann durch Auskommentieren (= % am Zeilenanfang) von **\printanswers** in der Präanbel verhindert werden.
  
- Eigene Befele sollten mit **my...** beginnen, um sie klar von vordefinierten Standardbefehlen unterscheiden zu können. LaTeX Studio bietet dann beim Eintippen von **\my** bereits alle Befehle mit entsprechendem Anfang zur Auswahl vor.
